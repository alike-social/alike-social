# alike-social

Repository for source code of Alike application

## Getting started
- To clone this repo: `git clone https://gitlab.com/alike-social/alike-social --recursive`  
- To pull the repo with changes on submodules: `git pull --recurse-submodule`  
- To pull only changes on submodules: `git pull --recurse-submodule `  

## Deploy and testing
In the root:  
`docker-compose up --build`  
Examples calls can be found on example/example_calls.json  
1. Create a user with username, password and email
2. Execute login call to get JWT
3. Copy that token and paste on token on resource call to test it


## Authors and acknowledgment
- Álvaro Quesada Sánchez
- Daniel Moreno Calvin
- Iker García Gutiérrez
- Javier Félix Montes Heras
- Sergio Almenzar Jiménez


## License
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
